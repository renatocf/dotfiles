"///////////////////////////////////////////////////////////////////////
"----------------------------------------------------------------------
"                                PYTHON  
"----------------------------------------------------------------------
"\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

"Identation
set tabstop=4 textwidth=80 
set noexpandtab

"Tabulações de python com cor
syn match pythonTAB '\t\+'

"Strings na cor verde
hi pythonString ctermfg=lightgreen
hi pythonRawString ctermfg=lightgreen 
