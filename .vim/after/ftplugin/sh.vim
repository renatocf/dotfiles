"///////////////////////////////////////////////////////////////////////
"----------------------------------------------------------------------
"                                 SHELL
"----------------------------------------------------------------------
"\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

set textwidth=0
    " Desativa a quebra de linhas ao final de um comando
    " (ideal para criar comandos extensos com várias opções)

set wrapmargin=0
    " Opção usada para quebra de linha caso textwidth=0
    " (ao colocá-la como 0 evita-se que haja quebra de linhas 
    " que não eram desejadas já ao desativar o textwidth)
