"++ MATEMÁTICA +++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    "Símbolos matemáticos de todos os tipos possíveis

"---- OPERADORES
iab _MAIS_MENOS_    ±
iab _TENSORIAL_     ⊗
iab _SOMA_DIRETA_   ⊕

"---- LÓGICA
iab _PARA_TODO_     ∀
iab _EXISTE_        ∃
iab _NAO_           ¬
iab _E_             ∧
iab _OU_            ∨
iab _PORTANTO_      ∴
iab _PORQUE_        ∵
iab _IMPLICA_       →
iab _SE_SO_SE_      ↔

"---- COMPARAÇÃO
iab _DIFERENTE_     ≠
iab _APROX_         ≈
iab _CONGRUENTE_    ≅
iab _EQUIVALENTE_   ≡
iab _MAIOR_IGUAL_   ≥
iab _MENOR_IGUAL_   ≤

"---- CÁLCULO
iab _FUNCAO_COMP_   ∘
iab _INFINITO_      ∞
iab _DIFERENCIAL_   ∂
iab _GRADIENTE_     ∇
iab _INTEGRAL_      ∫
iab _INTEG_LINHA_   ∮
iab _SOMATORIO_     ∑
iab _PRODUTORIO_    ∏

"---- CONJUNTOS
iab _VAZIO_         ∅
iab _NATURAIS_      ℕ
iab _INTEIROS_      ℤ
iab _RACIONAIS_     ℚ
iab _REAIS_         ℝ
iab _COMPLEXOS_     ℂ

"---- CONJUNTÍSTICA
iab _PERTENCE_      ∈
iab _NAO_PERTENCE_  ∉
iab _CONTEM_        ⊇
iab _CONTEM_DIF_    ⊃
iab _CONTIDO_       ⊆
iab _CONTIDO_DIF_   ⊂
iab _UNIAO_         U
iab _INTER_         ∩

"---- ÁLGEBRA LINEAR
iab _ORTOGONAL_     ⊥
iab _PROD_INTERNO_  ⟨⟩
